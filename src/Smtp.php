<?
namespace ITPolice\Mail;

use PHPMailer\PHPMailer\PHPMailer;

class Smtp
{

    public static function getConfig()
    {
        $config = array();
        $config['smtp_username'] = getenv("SMTP_USERNAME");  //Смените на адрес своего почтового ящика.
        $config['smtp_port'] = getenv("SMTP_PORT"); // Порт работы.
        $config['smtp_host'] = getenv("SMTP_HOST");  //сервер для отправки почты
        $config['smtp_password'] = getenv("SMTP_PASSWORD");  //Измените пароль
        $config['smtp_debug'] = getenv("SMTP_DEBUG");  //Если Вы хотите видеть сообщения ошибок, укажите true вместо false
        $config['smtp_charset'] = getenv("SMTP_CHARSET");    //кодировка сообщений. (windows-1251 или utf-8, итд)
        $config['smtp_from'] = getenv("SMTP_FROM"); //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле
        $config['smtp_from_name'] = getenv("SMTP_FROM_NAME"); //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле

        return $config;
    }

    public static function send($to, $subject, $message, $headers = [], $parameters = [])
    {
        $config = self::getConfig();

        if(!$to) return;

        //dd([$to, $subject, $message, $headers, $parameters]);

        $mail = new PHPMailer();
        $mail->SMTPDebug = $config['smtp_debug'];
        $mail->isSMTP();
        $mail->CharSet = $config['smtp_charset'];
        $mail->setLanguage('ru');
        // Set mailer to use SMTP
        $mail->Host = $config['smtp_host'];  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = $config['smtp_username'];; // SMTP username
        $mail->Password = $config['smtp_password'];; // SMTP password
        $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $config['smtp_port'];
        $mail->From = $config['smtp_from'];
        $mail->FromName = $config['smtp_from_name'];
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;
        //dd([$to, $subject, $message, $headers, $parameters]);
        foreach (explode(',', $to) as $recipient) {
            $mail->addAddress($recipient);
        }

        if (!$mail->send()) {
            throw new \Exception($mail->ErrorInfo);
        }
        $mail->clearAddresses();
        $mail->ClearCustomHeaders();

        return true;
    }
}